#ifndef SEGMENT_DISPLAY_HPP
#define SEGMENT_DISPLAY_HPP

#include <Arduino.h>
#include <TM1637Display.h>

class segment_display
{
  public:
    typedef unsigned char pin;
  
  protected:
      TM1637Display * display;
      volatile int current_number;
      int old_number;
      volatile bool colon;
      uint8_t current_light;

      pin pin_meter_light;

  public:
    segment_display(pin, int, TM1637Display *);
    void set_number(int);
    void set_light(unsigned);
    void set_colon(bool);
    int get_number();
    unsigned get_light();
    bool get_colon();
    void refresh_display();
};

#endif
