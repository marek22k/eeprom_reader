
#include "segment_display.hpp"

segment_display::segment_display(pin meter_light, int number, TM1637Display * display)
  : display(display), current_number(number), old_number(0), colon(false), current_light(0), pin_meter_light(meter_light)
{
  Serial.println("Initialize segment display...");
  this->refresh_display();
}


void segment_display::set_number(int number)
{
  this->current_number = number;
}

void segment_display::set_light(unsigned light)
{
  this->current_light = light;
}

void segment_display::set_colon(bool show)
{
  this->colon = show;
}

int segment_display::get_number()
{
  return this->current_number;
}

unsigned segment_display::get_light()
{
  return this->current_light;
}

bool segment_display::get_colon()
{
  return this->colon;
}

void segment_display::refresh_display()
{
  unsigned new_light = map(analogRead(this->pin_meter_light), 0, 1023, 0, 7);
  if (new_light != this->current_light || this->old_number != this->current_number)
  {
    this->old_number = this->current_number;
    this->current_light = new_light;
    this->display->setBrightness(this->current_light);
    this->display->showNumberDecEx(this->current_number, (this->colon ? 64 : 0), false, 4, 0);
  }
}
