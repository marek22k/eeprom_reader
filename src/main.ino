/* EEPROM Reader */
/*
    Copyright (C) 2020 Marek Küthe

    This library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* Includes */
#include <Arduino.h>
#include <eeprom_24c256.hpp>
#include <TM1637Display.h>
#include <segment_display.hpp>

/* Pins */
#define GREEN_LED 10

#define SEGMENT_DISPLAY_CLK 8
#define SEGMENT_DISPLAY_DIO 9

#define WRITE_BUTTON 6
#define UP_BUTTON 5
#define DOWN_BUTTON 4
#define READ_BUTTON 3

#define LIGHT_METER A3

/* I2C addresses */
#define EEPROM_ADDRESS 0b1010000

/* typedefs */
typedef unsigned char byte;

/* objects */
eeprom_24c256 eeprom(EEPROM_ADDRESS);
TM1637Display tm1637_display(SEGMENT_DISPLAY_CLK, SEGMENT_DISPLAY_DIO);
segment_display display(LIGHT_METER, 8888, &tm1637_display);

/* enums */
enum BUTTON { NONE, WRITE_B, UP, DOWN, READ_B };
enum MODE { ADDRESS_SELECT, READ_M, WRITE_M };

/* functions */
BUTTON read_buttons();
void mode_address_select(BUTTON);
void mode_read(BUTTON);
void initialize_mode_read();
void mode_write(BUTTON);
void initialize_mode_write();

/* variables */
MODE mode = ADDRESS_SELECT;
unsigned short address;
BUTTON last_pressed_button;
unsigned short write_value;

void setup()
{
    Serial.begin(9600);
    
    eeprom.init();
    
    pinMode(GREEN_LED, OUTPUT);
    
    pinMode(SEGMENT_DISPLAY_CLK, OUTPUT);
    pinMode(SEGMENT_DISPLAY_DIO, OUTPUT);
    
    pinMode(LIGHT_METER, INPUT);
    
    pinMode(WRITE_BUTTON, INPUT_PULLUP);
    pinMode(UP_BUTTON, INPUT_PULLUP);
    pinMode(DOWN_BUTTON, INPUT_PULLUP);
    pinMode(READ_BUTTON, INPUT_PULLUP);
    
    last_pressed_button = NONE;
    address = 0;
    
    display.set_number(0);
}

void loop()
{   
    BUTTON pressed_button = read_buttons();
    
    if (mode == ADDRESS_SELECT)
    {
        mode_address_select(pressed_button);
    }
    else if (mode == READ_M)
    {
        mode_read(pressed_button);
    }
    else if (mode == WRITE_M)
    {
        mode_write(pressed_button);
    }
    
    if (last_pressed_button == pressed_button && ( pressed_button == UP || pressed_button == DOWN ))
    {
        delay(10);
    }
    else
    {
        delay(300);
    }
    
    if (mode != ADDRESS_SELECT)
        digitalWrite(GREEN_LED, HIGH);
    else
        digitalWrite(GREEN_LED, LOW);
    
    last_pressed_button = pressed_button;
    display.refresh_display();
}

BUTTON read_buttons()
{   
    if (digitalRead(WRITE_BUTTON) == LOW)
    {
        return WRITE_B;
    }
    else if (digitalRead(UP_BUTTON) == LOW)
    {
        return UP;
    }
    else if (digitalRead(DOWN_BUTTON) == LOW)
    {
        return DOWN;
    }
    else if (digitalRead(READ_BUTTON) == LOW)
    {
        return READ_B;
    }
    
    return NONE;
}

void mode_address_select(BUTTON pressed_button)
{
    if (pressed_button == UP && address < 255)
    {
        address++;
    }
    else if (pressed_button == DOWN && address != 0)
    {
        address--;
    }
    else if (pressed_button == READ_B)
    {
        mode = READ_M;
        initialize_mode_read();
        return;
    }
    else if (pressed_button == WRITE_B)
    {
        mode = WRITE_M;
        initialize_mode_write();
        return;
    }
    display.set_number(address);
}

void mode_read(BUTTON pressed_button)
{
    if (pressed_button != NONE)
    {
        mode = ADDRESS_SELECT;
        return;
    }
}

void initialize_mode_read()
{
    byte value;
    bool successful = eeprom.read(address, &value);
    if (successful)
    {
        display.set_number(value);
    }
    else
    {
        display.set_number(-1);
        display.refresh_display();
        delay(2000);
        mode = ADDRESS_SELECT;
    }
}

void mode_write(BUTTON pressed_button)
{
    if (pressed_button == UP && write_value < 255)
    {
        write_value++;
    }
    else if (pressed_button == DOWN && write_value != 0)
    {
        write_value--;
    }
    else if (pressed_button == READ_B)
    {
        mode = ADDRESS_SELECT;
        return;
    }
    else if (pressed_button == WRITE_B)
    {
        mode = ADDRESS_SELECT;
        bool successful = eeprom.write(address, static_cast<byte>(write_value));
        if (! successful)
        {
            display.set_number(-1);
            delay(2000);
            display.refresh_display();
        }
        return;
    }
    display.set_number(write_value);
}

void initialize_mode_write()
{
    byte value;
    bool successful = eeprom.read(address, &value);
    if (successful)
    {
        write_value = static_cast<unsigned short>(value);
    }
    else
    {
        display.set_number(-1);
        display.refresh_display();
        delay(2000);
        mode = ADDRESS_SELECT;
    }
}
